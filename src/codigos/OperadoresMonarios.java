package variablesIncreibles;

public class OperadoresMonarios {

	public static void main(String[] args) {
		int a=0;
		System.out.println(a++);
		System.out.println(a);
		a=10;
		System.out.println(--a);
		a=7;
		System.out.println(a+=6);
		a=20;
		System.out.println(a-=5);
		a=28;
		System.out.println(a/=14);
		a=9;
		System.out.println(a%=4);
	}

}
